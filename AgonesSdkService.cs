using Microsoft.Extensions.Logging;
using Agones;
using System;
using System.Threading;
using System.Threading.Tasks;

// TODO:  https://stackoverflow.com/questions/55796612/net-core-difference-between-hosted-service-and-singleton-service
public class AgonesSdkService {
    private readonly ILogger<AgonesSdkService> _logger;

    private Timer _timer = null!;

    private AgonesSDK agonesSdk = null!;

    private int HEALTH_PING_PERIOD = 5; // seconds

    public AgonesSdkService(ILogger<AgonesSdkService> logger) {
        _logger = logger;
    }

    public Task StartAsync() {
        agonesSdk = new AgonesSDK();
        try {
            bool ok = agonesSdk.ConnectAsync().Result;
            //bool ok = false;
            if(!ok) {
                throw new Exception("NOT OK!");
            } 
        } catch(Exception e) {
            _logger.LogError("Failed to connect to Agones SDK during start up: ", e);
            return Task.FromException(e);
        }

        try {
            var status = agonesSdk.ReadyAsync().Result;
            //bool ok = false;
            if(status.StatusCode != Grpc.Core.StatusCode.OK) {
                throw new Exception(status.ToString());
            } 
        } catch(Exception e) {
            _logger.LogError("Failed to tell Agones Sdk that the game server is ready: ", e);
            return Task.FromException(e);
        }

        _timer = new Timer(SendHealthPing, null, TimeSpan.Zero, 
            TimeSpan.FromSeconds(HEALTH_PING_PERIOD));

        return Task.CompletedTask;
    }

    public async void SendHealthPing(object? state) {
        await agonesSdk.HealthAsync();
        _logger.LogInformation("Health Ping");
    }

    ~AgonesSdkService() {
        _timer?.Dispose();
        agonesSdk?.Dispose();
    }
}