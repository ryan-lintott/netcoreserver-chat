using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using NetCoreServer;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;


namespace NetCoreServerChatNS {

    public class Command {
        public string type;
        public string roomId;
        public string user;
        public string message;
    }

    class ChatSession : WssSession {
        public ChatSession(WssServer server, 
                           ILogger<ChatServer> logger, 
                           RoomsManager roomsManager) : base(server) {
            _logger = logger;
            _roomsManager = roomsManager;
        }

        private ILogger<ChatServer> _logger;

        private RoomsManager _roomsManager;


        public override void OnWsConnected(HttpRequest request) {
        }

        public override void OnWsDisconnected() {
           
           _logger.LogInformation("disconnected!!!!!");
            byte b = 0;
            _roomsManager.currentConnections.TryRemove(Id, out b);
            string? roomId = _roomsManager.RemoveConnection(Id);
            if(roomId != null) {
                var sessions = _roomsManager.GetConnections(roomId);
                if(sessions != null) {
                    // await Clients.Clients(sessions)
                    //          .SendAsync("AdminMessage", $"{Context.ConnectionId} disconnected");
                }
            } 
        }

        public override void OnWsReceived(byte[] buffer, long offset, long size) {
            
            string message = Encoding.UTF8.GetString(buffer, (int)offset, (int)size);
            Command command = new Command();
            command.type = "ReceiveMessage";

            // Multicast message to all connected sessions
            var received = JsonConvert.DeserializeObject<Command>(message);
            List<System.Guid> connections = _roomsManager.GetConnections(received.roomId);

            if(received.type.Equals("Send")) {
                command.message = received.message;
                command.user = received.user;
                command.roomId = received.roomId;

                foreach(Guid conn in connections) {
                    try {

                        SslSession session =  (((WssServer)Server).FindSession(conn));
                        if(session != null && !session.IsDisposed) {
                            ((WssSession)session).SendText(JsonConvert.SerializeObject(command));
                        }
                                    
                    } catch(Exception e) {
                        // Console.WriteLine(e.Message);
                        _logger.LogWarning(e.Message);
                    }
                }

            } else if((received.type.Equals("JoinRoom"))) {
                _roomsManager.AddOrUpdatePlayer(received.roomId, 
                                                received.user, 
                                                Id);
            } else {
                // TODO: error or throw exception
            }
        }

        protected override void Dispose(bool disposingManagedResources)  {
            base.Dispose(disposingManagedResources);
        }

        protected override void OnError(SocketError error) {
            //Console.WriteLine($"Chat WebSocket session caught an error with code {error}");
            _logger.LogError($"Chat WebSocket session caught an error with code {error}");
        }


    }

    class ChatServer : WssServer {
        public ChatServer(SslContext context, IPAddress address, 
                          int port, 
                          ILogger<ChatServer> logger, 
                          RoomsManager roomsManager) : base(context, address, port) {
            _logger = logger;
            _roomsManager = roomsManager;
        }

        private ILogger<ChatServer> _logger;

        private RoomsManager _roomsManager;
        
        protected override WssSession CreateSession() { return new ChatSession(this, _logger, _roomsManager); }
        protected override void OnError(SocketError error) {
            //Console.WriteLine($"Chat WebSocket server caught an error with code {error}");
            _logger.LogError($"Chat WebSocket server caught an error with code {error}");
        }

        // protected override void OnDisconnected(WssSession session)
        // {
        //     base.OnDisconnected(session);
        //     ((ChatSession)session).Dispose();
        // }

    }
}