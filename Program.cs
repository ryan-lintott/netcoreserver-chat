using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using System.Net;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using NetCoreServer;

namespace ChatSample {
    public class Program {
        public static int Main(string[] args) {
            var loggerFactory = LoggerFactory.Create(builder => {
                builder.AddConsole();
            });

            var logger = loggerFactory.CreateLogger<Program>();
            AgonesSdkService agonesSdkService = new AgonesSdkService(loggerFactory.CreateLogger<AgonesSdkService>());

            agonesSdkService.StartAsync().Wait();

            RoomsManager roomsManager = new RoomsManager();

            for(int i = 0; i < 50; i++) {
                roomsManager.CreateRoom();
            }
            
            var context = new SslContext(SslProtocols.Tls12, 
                                        X509Certificate2.CreateFromEncryptedPemFile("wss/cert.pem", "qwerty", "wss/key.pem"));

            var server = new NetCoreServerChatNS.ChatServer(context,
                                                            IPAddress.Any, 
                                                            5000, 
                                                            loggerFactory.CreateLogger<NetCoreServerChatNS.ChatServer>(), 
                                                            roomsManager);
            
            server.Start();
            logger.LogInformation("started netcore server...");
            Task.Delay(-1).Wait();

            // 

            return 0;
        }
    }
}