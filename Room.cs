using System.Collections.Concurrent;

class Room {

    // TODO: the c# get, set syntax
    ConcurrentDictionary<string, System.Guid> playerIdToConnectionId = new ConcurrentDictionary<string, System.Guid>();
    // TODO: connectionId to playerId

    public const int maxPlayers = 4;

    public int GetPlayerCount() {
        return playerIdToConnectionId.Count;
    }

    public bool AddOrUpdatePlayer(string playerId, System.Guid connectionId) {
        //lock(playerIdToConnectionId) {
            if(playerIdToConnectionId.Count >= maxPlayers && !playerIdToConnectionId.ContainsKey(playerId)) {
                // TODO: custom exceptions maybe
                throw new Exception("exceeded max player count");
            }
            playerIdToConnectionId[playerId] = connectionId;   
        //}
        return true;
    }

    public bool RemoveConnection(System.Guid connectionId) {
        bool result = false;

        foreach(var keyValue in playerIdToConnectionId) {
            if(keyValue.Value.Equals(connectionId)) {
                // TODO: remove player
                result = playerIdToConnectionId.TryUpdate(keyValue.Key, System.Guid.Empty, connectionId);
                break;
            }
        }
        return result;
    }

    public List<System.Guid> GetConnections() {
        return playerIdToConnectionId.Values.ToList();
    }
}