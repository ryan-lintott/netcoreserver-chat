
using System.Collections.Concurrent;
using System.Threading;
using System.Net.Http;
using System.Net;
using Microsoft.Extensions.Logging;
using System.IO;

public class RoomsManager {

    int counter = 0;

    int roomLimit;

    public int RoomLimit {
        get { return roomLimit; }
    }

    private ConcurrentDictionary<string, Room> rooms = new ConcurrentDictionary<string, Room>();
    
    public ConcurrentDictionary<System.Guid, string> connectionIdToRoomId = new ConcurrentDictionary<System.Guid, string>();

    // TODO: get logging working with unit tests
    //private readonly ILogger<RoomsManager> _logger;
    // private Timer? _timer = null;

    private int connectionIdCleanupPeriod = 300; // seconds
    
    public ConcurrentDictionary<System.Guid, byte> currentConnections = new ConcurrentDictionary<System.Guid, byte>();

    public RoomsManager() {
        if(!Int32.TryParse(Environment.GetEnvironmentVariable("MAX_ROOMS"), out roomLimit)) {
            roomLimit = 100;
        }
    }

    public void AddOrUpdatePlayer(string roomId, string playerId, System.Guid connectionId) {
        Room? room = null;
        rooms.TryGetValue(roomId, out room);
        
        string? connectionRoomId = null;

        //lock(connectionIdToRoomId) {
            connectionIdToRoomId.TryGetValue(connectionId, out connectionRoomId);
            
            // TODO, add exceptions 
            if(connectionRoomId != null && !connectionRoomId.Equals(roomId)) {
                throw new Exception($"Player is already in a different room, (room {connectionRoomId})");
            }
            
            if(room != null) {
                bool succeeded = room.AddOrUpdatePlayer(playerId, connectionId);
                if(succeeded) {
                    connectionIdToRoomId[connectionId] = roomId;
                }
            } else {
                //return false;
            }
        //}
    }

    public string? RemoveConnection(System.Guid connectionId) {
        string? roomId = null;
        bool success = false;
        //lock(connectionIdToRoomId) {
            connectionIdToRoomId.TryGetValue(connectionId, out roomId);
            if(roomId != null) {
                Room? room = null;
                rooms.TryGetValue(roomId, out room);
                if(room != null) {
                    success = room.RemoveConnection(connectionId);
                }
                connectionIdToRoomId.TryRemove(connectionId, out roomId);
            } 
        //}
        if(success) {
            return roomId;
        } else {
            return null;
        }
    }

    public List<System.Guid>? GetConnections(string roomId) {
        Room? room = null;
        //rooms.ContainsKey(roomId);
        rooms.TryGetValue(roomId, out room);
        if(room != null) {
            return room.GetConnections();
        } else {
            return null;
        }
    }

    public string CreateRoom() {
        int newVal = Interlocked.Add(ref counter, 1);
        if(newVal > roomLimit) {
            // TODO custom exception
            throw new Exception("exceeded max number of rooms");
        }
        rooms.TryAdd(newVal.ToString(), new Room());
        return newVal.ToString();
    }

    public bool DestroyRoom(string roomId) {
        Room? room = null;
        return rooms.TryRemove(roomId, out room);
    }


}