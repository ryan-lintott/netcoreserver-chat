using Xunit;

namespace RoomManagerTests {

    // TODO remember to test specification!

    public class RoomManagerTests {

        RoomsManager roomsManager = new RoomsManager();

        [Fact]
        public void ShouldHaveFourConnectionsAfterFourPlayersEnterRoom() {
            string roomId = roomsManager.CreateRoom();

            System.Guid id1 = System.Guid.NewGuid();
            System.Guid id2 = System.Guid.NewGuid();
            System.Guid id3 = System.Guid.NewGuid();
            System.Guid id4 = System.Guid.NewGuid();

            roomsManager.AddOrUpdatePlayer(roomId, "chris", id1);
            roomsManager.AddOrUpdatePlayer(roomId, "bob", id2);
            roomsManager.AddOrUpdatePlayer(roomId, "ryan", id3);
            roomsManager.AddOrUpdatePlayer(roomId, "kate", id4);

            List<System.Guid>? connections = roomsManager.GetConnections(roomId);
            List<System.Guid> expected = new List<System.Guid> { id1, id2, id3, id4 };
            
            Assert.True(connections!.All(expected.Contains) && connections!.Count == expected.Count);
        }

        [Fact]
        public void ShouldHaveThreeConnectionsAfterFourPlayersEnterAndOneConnectionLeaves() {
            string roomId = roomsManager.CreateRoom();

            System.Guid id1 = System.Guid.NewGuid();
            System.Guid id2 = System.Guid.NewGuid();
            System.Guid id3 = System.Guid.NewGuid();
            System.Guid id4 = System.Guid.NewGuid();

            roomsManager.AddOrUpdatePlayer(roomId, "chris", id1);
            roomsManager.AddOrUpdatePlayer(roomId, "bob", id2);
            roomsManager.AddOrUpdatePlayer(roomId, "ryan", id3);
            roomsManager.AddOrUpdatePlayer(roomId, "kate", id4);

            roomsManager.RemoveConnection(id4);

            List<System.Guid>? connections = roomsManager.GetConnections(roomId);
            List<System.Guid> expected = new List<System.Guid> { id1, id2, id3, System.Guid.Empty };
            
            Assert.True(connections!.All(expected.Contains) && connections!.Count == expected.Count);
        }

        [Fact]
        public void ShouldHaveOneConnectionInEachRoomAfterJoiningDifferentRooms() {
            string roomId1 = roomsManager.CreateRoom();
            string roomId2 = roomsManager.CreateRoom();

            System.Guid id1 = System.Guid.NewGuid();
            System.Guid id2 = System.Guid.NewGuid();

            roomsManager.AddOrUpdatePlayer(roomId1, "kate", id1);
            roomsManager.AddOrUpdatePlayer(roomId2, "ryan", id2);

            var room1Conns = roomsManager.GetConnections(roomId1);
            var room2Conns = roomsManager.GetConnections(roomId2);
            List<System.Guid> expected1 = new List<System.Guid> { id1 };
            List<System.Guid> expected2 = new List<System.Guid> { id2 };

            Assert.True(room1Conns!.All(expected1.Contains) && room1Conns!.Count == expected1.Count);
            Assert.True(room2Conns!.All(expected2.Contains) && room2Conns!.Count == expected2.Count);
        }

        [Fact]
        public void ConnectionShouldUpdate() {
            string roomId = roomsManager.CreateRoom();

            System.Guid id1 = System.Guid.NewGuid();
            System.Guid id2 = System.Guid.NewGuid();

            roomsManager.AddOrUpdatePlayer(roomId, "ryan", id1);
            roomsManager.AddOrUpdatePlayer(roomId, "ryan", id2);
            var connections = roomsManager.GetConnections(roomId);
            List<System.Guid> expected = new List<System.Guid> { id2 };

            Assert.True(connections!.All(expected.Contains) && connections!.Count == expected.Count);
        }

        [Fact]
        public void ConnectionShouldUpdateInOneRoomOnly() {
            string roomId1 = roomsManager.CreateRoom();
            string roomId2 = roomsManager.CreateRoom();

            System.Guid id1 = System.Guid.NewGuid();
            System.Guid id2 = System.Guid.NewGuid();
            System.Guid id3 = System.Guid.NewGuid();

            roomsManager.AddOrUpdatePlayer(roomId1, "kate", id1);
            roomsManager.AddOrUpdatePlayer(roomId2, "ryan", id2);

            roomsManager.AddOrUpdatePlayer(roomId1, "kate", id3);

            var room1Conns = roomsManager.GetConnections(roomId1);
            var room2Conns = roomsManager.GetConnections(roomId2);
            List<System.Guid> expected1 = new List<System.Guid> { id3 };
            List<System.Guid> expected2 = new List<System.Guid> { id2 };

            Assert.True(room1Conns!.All(expected1.Contains) && room1Conns!.Count == expected1.Count);
            Assert.True(room2Conns!.All(expected2.Contains) && room2Conns!.Count == expected2.Count);
        }

        [Fact]
        public void ConnectionShouldBeDeletedInOneRoomOnly() {
            string roomId1 = roomsManager.CreateRoom();
            string roomId2 = roomsManager.CreateRoom();

            System.Guid id1 = System.Guid.NewGuid();
            System.Guid id2 = System.Guid.NewGuid();

            roomsManager.AddOrUpdatePlayer(roomId1, "kate", id1);
            roomsManager.AddOrUpdatePlayer(roomId2, "ryan", id2);

            roomsManager.RemoveConnection(id2);

            var room1Conns = roomsManager.GetConnections(roomId1);
            var room2Conns = roomsManager.GetConnections(roomId2);
            List<System.Guid> expected1 = new List<System.Guid> { id1 };
            List<System.Guid> expected2 = new List<System.Guid> { System.Guid.Empty };

            Assert.True(room1Conns!.All(expected1.Contains) && room1Conns!.Count == expected1.Count);
            Assert.True(room2Conns!.All(expected2.Contains) && room2Conns!.Count == expected2.Count);
        }

        [Fact]
        public void ShouldNotBeAbleToChangeTheRoomOfAConnection() {
            string roomId1 = roomsManager.CreateRoom();
            string roomId2 = roomsManager.CreateRoom();

            System.Guid id1 = System.Guid.NewGuid();

            roomsManager.AddOrUpdatePlayer(roomId1, "ryan", id1);

            Exception ex = Assert.Throws<Exception>(() => roomsManager.AddOrUpdatePlayer(roomId2, "ryan", id1));
            Assert.Equal($"Player is already in a different room, (room {roomId1})", ex.Message);
        }

        [Fact]
        public void ShouldNotAllowMaxPlayersPerRoomToBeExceeded() {
            string roomId = roomsManager.CreateRoom();

            System.Guid id1 = System.Guid.NewGuid();
            System.Guid id2 = System.Guid.NewGuid();
            System.Guid id3 = System.Guid.NewGuid();
            System.Guid id4 = System.Guid.NewGuid();
            System.Guid id5 = System.Guid.NewGuid();

            roomsManager.AddOrUpdatePlayer(roomId, "ryan", id1);
            roomsManager.AddOrUpdatePlayer(roomId, "bob", id2);
            roomsManager.AddOrUpdatePlayer(roomId, "kate", id3);
            roomsManager.AddOrUpdatePlayer(roomId, "mary", id4);

            Exception ex = Assert.Throws<Exception>(() => roomsManager.AddOrUpdatePlayer(roomId, "chris", id5));
            Assert.Equal($"exceeded max player count", ex.Message);
        }

        [Fact]
        public void ShouldNotAllowMaxPlayersPerRoomToBeExceededSameConnectionId() {
            string roomId = roomsManager.CreateRoom();
            
            System.Guid id1 = System.Guid.NewGuid();
            System.Guid id2 = System.Guid.NewGuid();
            System.Guid id3 = System.Guid.NewGuid();
            System.Guid id4 = System.Guid.NewGuid();

            roomsManager.AddOrUpdatePlayer(roomId, "ryan", id1);
            roomsManager.AddOrUpdatePlayer(roomId, "bob", id2);
            roomsManager.AddOrUpdatePlayer(roomId, "kate", id3);
            roomsManager.AddOrUpdatePlayer(roomId, "mary", id4);

            Exception ex = Assert.Throws<Exception>(() => roomsManager.AddOrUpdatePlayer(roomId, "chris", id4));
            Assert.Equal($"exceeded max player count", ex.Message);
        }

        [Fact]
        public void ShouldUpdatePlayerConnection() {
            string roomId = roomsManager.CreateRoom();

            System.Guid id1 = System.Guid.NewGuid();
            System.Guid id2 = System.Guid.NewGuid();

            roomsManager.AddOrUpdatePlayer(roomId, "ryan", id1);
            roomsManager.AddOrUpdatePlayer(roomId, "ryan", id2);
            var connections = roomsManager.GetConnections(roomId);
            List<System.Guid> expected = new List<System.Guid> { id2 };

            Assert.True(connections!.All(expected.Contains) && connections!.Count == expected.Count);
        }

        [Fact]
        public void ShouldNotAllowMaxRoomsToBeExceeded() {
            for(int i = 0; i < roomsManager.RoomLimit; i++) {
                string roomId = roomsManager.CreateRoom();
            }

            Exception ex = Assert.Throws<Exception>(() => roomsManager.CreateRoom());
            Assert.Equal($"exceeded max number of rooms", ex.Message);
        }

        [Fact]
        public void ShouldAllowPlayerToUpdateConnectionWhenRoomFull() {
            string roomId = roomsManager.CreateRoom();

            System.Guid id1 = System.Guid.NewGuid();
            System.Guid id2 = System.Guid.NewGuid();
            System.Guid id3 = System.Guid.NewGuid();
            System.Guid id4 = System.Guid.NewGuid();
            System.Guid id5 = System.Guid.NewGuid();

            roomsManager.AddOrUpdatePlayer(roomId, "chris", id1);
            roomsManager.AddOrUpdatePlayer(roomId, "bob", id2);
            roomsManager.AddOrUpdatePlayer(roomId, "ryan", id3);
            roomsManager.AddOrUpdatePlayer(roomId, "kate", id4);

            roomsManager.AddOrUpdatePlayer(roomId, "kate", id5);

            List<System.Guid>? connections = roomsManager.GetConnections(roomId);
            List<System.Guid> expected = new List<System.Guid> { id1, id2, id3, id5 };
            
            Assert.True(connections!.All(expected.Contains) && connections!.Count == expected.Count);
        }
        

        [Fact]
        public void ShouldAllowPlayerToUpdateConnectionWhenRoomFullSameConnectionId() {
            string roomId = roomsManager.CreateRoom();

            System.Guid id1 = System.Guid.NewGuid();
            System.Guid id2 = System.Guid.NewGuid();
            System.Guid id3 = System.Guid.NewGuid();
            System.Guid id4 = System.Guid.NewGuid();

            roomsManager.AddOrUpdatePlayer(roomId, "chris", id1);
            roomsManager.AddOrUpdatePlayer(roomId, "bob", id2);
            roomsManager.AddOrUpdatePlayer(roomId, "ryan", id3);
            roomsManager.AddOrUpdatePlayer(roomId, "kate", id4);

            roomsManager.AddOrUpdatePlayer(roomId, "kate", id4);

            List<System.Guid>? connections = roomsManager.GetConnections(roomId);
            List<System.Guid> expected = new List<System.Guid> { id1, id2, id3, id4 };
            
            Assert.True(connections!.All(expected.Contains) && connections!.Count == expected.Count);
        }

    }
}